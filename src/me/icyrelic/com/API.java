package me.icyrelic.com;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class API {
	
	private File PlayersFile = new File(LegendaryVoting.getInstance().getDataFolder(),"players.yml");
	private FileConfiguration conf = YamlConfiguration.loadConfiguration(PlayersFile);
	
	public int getTokenBalance(Player p){
		return conf.getInt(p.getUniqueId() + ".Tokens");
	}
	
	public void subtractTokens(Player p, int tokens){
		int has = conf.getInt(p.getUniqueId() + ".Tokens");
		int total = has-tokens;
		
		conf.set(p.getUniqueId()+".Tokens", total);
		try {
			conf.save(PlayersFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addTokens(Player p, int tokens){
		int has = conf.getInt(p.getUniqueId() + ".Tokens");
		int total = has+tokens;
		
		conf.set(p.getUniqueId()+".Tokens", total);
		try {
			conf.save(PlayersFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
