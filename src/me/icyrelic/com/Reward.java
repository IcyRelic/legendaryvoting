package me.icyrelic.com;

import me.icyrelic.com.Enum.Rarity;
import me.icyrelic.com.Enum.RewardType;

public class Reward {
	
	private String name;
	private Rarity rarity;
	private RewardType type;
	private String value;
	
	public Reward(String name, Rarity rarity, RewardType type, String value){
		this.name = name;
		this.rarity = rarity;
		this.type = type;
		this.value = value;
		
	}
	
	public String getName(){
		return this.name;
	}
	
	public Rarity getRarity(){
		return this.rarity;
	}
	
	public RewardType getType(){
		return this.type;
	}
	
	public String getValue(){
		return this.value;
	}

	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("|");
		sb.append(rarity.toString());
		sb.append("|");
		sb.append(type.toString());
		sb.append("|");
		sb.append(value);
		
		return sb.toString();
	}
}
