package me.icyrelic.com.Enum;

public enum Rarity {

	COMMON(40), UNCOMMON(30), RARE(10), ULTRA_RARE(7), EPIC(3);
	
	private int prob;
	Rarity(int prob) { this.prob = prob; }
	public int getProbability() { return prob; }
	public void setProbability(int x) { prob = x; }
	
}
