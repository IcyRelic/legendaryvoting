package me.icyrelic.com;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import me.icyrelic.com.Enum.Rarity;
import me.icyrelic.com.Enum.RewardType;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Util {
	
	private static void giveMoney(Player p, Double money){
		String uid = PUUID.getUUID(p.getName());
		LegendaryVoting.economy.depositPlayer(Bukkit.getOfflinePlayer(UUID.fromString(uid)), money);
	}
	
	private static void giveItems(Player p, String[] str_items){
		for(String str_item : str_items){
			String[] split = str_item.split(":");
			Material mat = Material.valueOf(split[0]);
			int amount = Integer.parseInt(split[1]);
			short data = Short.parseShort(split[2]);
			ItemStack item = new ItemStack(mat,amount,data);
			p.getInventory().addItem(item);
		}
	}
	
	private static void runCommands(Player p, String[] commands){
		
		for(String cmd : commands){
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmd);
		}
		
	}
	
	private static void giveRank(Player p, String rank){
		String rankCommand = LegendaryVoting.getInstance().getConfig().getString("Settings.RankCommand")
				.replace("%name%", p.getName())
				.replace("%rank%", rank);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), rankCommand);
	}
	
	private static void giveTokens(Player p, int tokens){
		LegendaryVoting.getInstance().getAPI().addTokens(p, tokens);
	}

	private static void broadcast(Player p, String personal, String global){
		Bukkit.getServer().broadcastMessage(global);
		p.sendMessage(personal);
	}
	
	public static void addVote(Player p, String[] messages){
		broadcast(p, messages[0], messages[1]);
		Rarity rarity = selectRarity();
		Reward reward = selectReward(rarity);
		System.out.println("adding " + reward.getName() + " for player " + p.getName());
		addReward(p, reward);
		
	}
	
	public static void addReward(Player p, Reward reward){
		boolean contains = LegendaryVoting.getInstance().pendingVotes.containsKey(p.getUniqueId());
		List<Reward> rewards = contains ? LegendaryVoting.getInstance().pendingVotes.get(p.getUniqueId()) : new ArrayList<Reward>();
		
		rewards.add(reward);
		
		if(contains)
			LegendaryVoting.getInstance().pendingVotes.remove(p.getUniqueId());
		
		LegendaryVoting.getInstance().pendingVotes.put(p.getUniqueId(), rewards);
		
	}
	
	public static void completeVote(UUID uid){
		boolean contains = LegendaryVoting.getInstance().pendingVotes.containsKey(uid);
		if(!contains)
			return;
		
		List<Reward> rewards = LegendaryVoting.getInstance().pendingVotes.get(uid);
		for(Reward reward : rewards){
			
			Player p = Bukkit.getPlayer(uid);
			RewardType type = reward.getType();
			
			p.sendMessage("You have been given an " + reward.getRarity().toString() + " reward! This reward is: " + type.toString());
			
			switch(type){
				case ITEM:
					giveItems(p, reward.getValue().split(","));
					break;
				case COMMAND:
					runCommands(p, reward.getValue().split(","));
					break;
				case RANK:
					giveRank(p,reward.getValue());
					break;
				case MONEY:
					giveMoney(p, Double.parseDouble(reward.getValue()));
					break;
				case TOKEN:
					giveTokens(p, Integer.parseInt(reward.getValue()));
					break;
			}
		}
		
		LegendaryVoting.getInstance().pendingVotes.remove(uid);
		
	}
	
	private static Rarity selectRarity(){
		List<Rarity> raritySelector = new ArrayList<Rarity>();
		
		for(Rarity rarity : Rarity.values()){
			int x = 0;
			while(x <= rarity.getProbability()){
				x++;
				raritySelector.add(rarity);
			}
		}
		
		Random rand = new Random();
		return raritySelector.get(rand.nextInt(raritySelector.size()));
		
	}
	
	public static Reward selectReward(Rarity rarity){
		List<Reward> rewards = new ArrayList<Reward>();
		
		for(Reward reward : LegendaryVoting.getInstance().rewards){
			if(reward.getRarity() == rarity)
				rewards.add(reward);
		}
		
		Random rand = new Random();
		
		return rewards.get(rand.nextInt(rewards.size()));
		
		
	}

	public static void loadProbabilities(){
		
		for(Rarity rarity : Rarity.values()){
			int prob = LegendaryVoting.getInstance().getConfig().getInt("Settings.Probability." + rarity.toString());
			rarity.setProbability(prob);
			
			if(LegendaryVoting.getInstance().debug)
				System.out.println(rarity.toString() + " Probability: " + prob);
			
		}
		
	}
	
	public static void loadRewards(){
		File RewardsFile;
		RewardsFile = new File(LegendaryVoting.getInstance().getDataFolder(),"rewards.yml");
     	FileConfiguration conf = YamlConfiguration.loadConfiguration(RewardsFile);
     	
     	for (String key : conf.getConfigurationSection("Rewards").getKeys(false)) {
     	
     		Rarity rarity = Rarity.valueOf(conf.getString("Rewards."+key+".Rarity"));
     		RewardType type = RewardType.valueOf(conf.getString("Rewards."+key+".Type"));
     		String value = conf.getString("Rewards."+key+".Value");
     		
     		Reward reward = new Reward(key, rarity, type, value);
     		
     		LegendaryVoting.getInstance().rewards.add(reward);
     		
     		if(LegendaryVoting.getInstance().debug)
     			System.out.println("Reward Loaded: " + reward.toString());
     	}
	}
}
