package me.icyrelic.com.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import me.icyrelic.com.LegendaryVoting;

import org.bukkit.entity.Player;

public class Database {
	
	static Connection con = null;
    static Statement st = null;
    static ResultSet rs = null;
	
	static LegendaryVoting plugin;
	public Database(LegendaryVoting instance) {

		plugin = instance;

		}
	
	public static boolean connect(String host, String port, String db, String user, String password){
		

	    String url = "jdbc:mysql://"+ host + ":" + port + "/" + db;
		 try {
			con = DriverManager.getConnection(url, user, password);
			 st = con.createStatement();
			 try{
				  Statement st = con.createStatement();
				  String table = 
						  "CREATE TABLE votes " +
				                   "(id INTEGER NOT NULL AUTO_INCREMENT, " +
				                   " user VARCHAR(255), " + 
				                   " votes INTEGER(255), " + 
				                   " PRIMARY KEY ( id ))";
				  st.executeUpdate(table);
			 }
			 catch(SQLException s){
			 }
			 return true;
			 
		} catch (SQLException e) {
			return false;
		}
	}
	
	
	public static void addVote(Player p){
		

        try {
           
            rs = st.executeQuery("SELECT votes FROM votes WHERE user = '"+p.getName()+"'");
            
            if (rs.next()) {
            	int x = Integer.parseInt(rs.getString(1));
            	int y = x+1;
            		st.executeUpdate("UPDATE votes SET votes='"+y+"' WHERE user = '"+p.getName()+"'");
            }else{
            	st.executeUpdate("INSERT INTO votes (id, user, votes) VALUES (NULL, '"+p.getName()+"', 1)");
            }


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
		
	}
	
	public static String selectWinner(){

        try {
           
            rs = st.executeQuery("SELECT * FROM votes ORDER BY RAND() LIMIT 0, 1");
            
            if (rs.next()) {
            		return rs.getString(0);
            }else{
            	return "No Winner";
            }


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return "No Winner";

        }
	}
	
	public static void setWinner(String winner){
		
		try {
	           
            rs = st.executeQuery("SELECT user FROM winner WHERE id = '1'");
            
            if (rs.next()) {
            	st.executeUpdate("UPDATE winner SET user='"+winner+"' WHERE id = '1'");
            }else{
            	System.out.println("Error Setting Winner To: " + winner);
            }


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
		
	}
	
	
	
	public static String getWinner(){

        try {
           
            rs = st.executeQuery("SELECT user FROM winner WHERE id = '1'");
            
            if (rs.next()) {
            		return rs.getString(0);
            }else{
            	return "No Winner";
            }


        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return "No Winner";

        }
	}
	
	

}
