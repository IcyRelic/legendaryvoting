package me.icyrelic.com.Listeners;

import java.io.File;
import java.util.UUID;

import me.icyrelic.com.LegendaryVoting;
import me.icyrelic.com.Util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {
	
	LegendaryVoting plugin;
	public PlayerJoin(LegendaryVoting instance) {
		plugin = instance;
	}
	
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		
		if(plugin.debug){
			String MSG = plugin.getConfig().getString("Messages.PlayerOnly").replace("%service%", "DEBUG").replace("%player%", "DEBUG").replaceAll("(&([a-f0-9]))", "\u00A7$2");
	    	String BCAST = plugin.getConfig().getString("Messages.Broadcast").replace("%service%", "DEBUG").replace("%player%", "DEBUG").replaceAll("(&([a-f0-9]))", "\u00A7$2");
	    	String[] messages = {MSG, BCAST};
	    	Util.addVote(e.getPlayer(), messages);
	    	
	    	System.out.println(plugin.getAPI().getTokenBalance(e.getPlayer()));
	    	plugin.getAPI().addTokens(e.getPlayer(), 5);
	    	System.out.println(plugin.getAPI().getTokenBalance(e.getPlayer()));
	    	plugin.getAPI().subtractTokens(e.getPlayer(), 2);
	    	System.out.println(plugin.getAPI().getTokenBalance(e.getPlayer()));
	    	
		}
		
		File PlayersFile;
		PlayersFile = new File(LegendaryVoting.getInstance().getDataFolder(),"players.yml");
     	FileConfiguration conf = YamlConfiguration.loadConfiguration(PlayersFile);
		
     	
     	if(conf.getString(e.getPlayer().getUniqueId()+".Tokens") == null){
     		conf.set(e.getPlayer().getUniqueId()+".Tokens", 0);
     	}
     	
		UUID uid = e.getPlayer().getUniqueId();
		Util.completeVote(uid);
	}

}
