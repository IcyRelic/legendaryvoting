package me.icyrelic.com.Listeners;

import java.util.UUID;

import me.icyrelic.com.LegendaryVoting;
import me.icyrelic.com.PUUID;
import me.icyrelic.com.Util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteListener implements Listener{
	
	LegendaryVoting plugin;
	public VoteListener(LegendaryVoting instance) {

		plugin = instance;

		
	}
	
	@EventHandler
    public void onVotifierEvent(VotifierEvent event) {
        Vote vote = event.getVote();
        String user = vote.getUsername();
    	String service = vote.getServiceName();
		Player p = (Player) Bukkit.getPlayer(UUID.fromString(PUUID.getUUID(user)));
    	String MSG = plugin.getConfig().getString("Messages.PlayerOnly").replace("%service%", service).replace("%player%", user).replaceAll("(&([a-f0-9]))", "\u00A7$2");
    	String BCAST = plugin.getConfig().getString("Messages.Broadcast").replace("%service%", service).replace("%player%", user).replaceAll("(&([a-f0-9]))", "\u00A7$2");
    	
    	String[] messages = {MSG, BCAST};
    	Util.addVote(p, messages);
	 }
	 
	 
	 
	 
	 

}
