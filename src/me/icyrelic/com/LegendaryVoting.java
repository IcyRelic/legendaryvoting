package me.icyrelic.com;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.icyrelic.com.Data.Database;
import me.icyrelic.com.Listeners.PlayerJoin;
import me.icyrelic.com.Listeners.VoteListener;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class LegendaryVoting extends JavaPlugin{
	
	public HashMap<UUID, List<Reward>> pendingVotes = new HashMap<UUID, List<Reward>>();
	public List<Reward> rewards = new ArrayList<Reward>();
	public static Economy economy = null;
	public boolean database = false;
	public boolean debug = false;
	private API api;
	private static LegendaryVoting instance;
	
	public static LegendaryVoting getInstance(){
		return instance;
	}
	
	public API getAPI(){
		return this.api;
	}
	
	@Override
	public void onEnable(){
		instance = this;
		loadConfiguration();
		setupEconomy();
		setupDatabase();
		loadListeners();
		Util.loadProbabilities();
		Util.loadRewards();
		api = new API();
	}
	
	private void loadListeners(){
		Bukkit.getServer().getPluginManager().registerEvents(new VoteListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);
	}
	
	private void setupDatabase(){
	    if(getConfig().getBoolean("Database.Enabled")){
	    	database = true;
			String host = getConfig().getString("Database.Host");
		    String port = getConfig().getString("Database.Port");
		    String db = getConfig().getString("Database.Database");
		    String user = getConfig().getString("Database.Username");
		    String password = getConfig().getString("Database.Password");
	    	if(!Database.connect(host, port, db, user, password)){
	    		database = false;
	    		System.out.println("[LegendaryVoting] FAILED TO CONNECT TO DATABASE");
	    	}
	    }
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	    File rewards = new File(getDataFolder(), "rewards.yml");
	    if(!rewards.exists())
	    	saveResource("rewards.yml", false);
	    
	    File players = new File(getDataFolder(), "players.yml");
	    if(!players.exists())
	    	saveResource("players.yml", false);
	}
	
    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

}
